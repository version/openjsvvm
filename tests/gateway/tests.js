var URI = "http://localhost:4321";
QUnit.module("gateway client")

QUnit.test( "gateway client initialization", function( assert ) {
  var gateway = new Gateway.Gateway();
  gateway.init(URI);
  assert.ok(gateway.doesConnectionEstablished(), "Gateway connection established");

});