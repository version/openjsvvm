var REGISTERS_VOLUME = 64;
// TODO remove argument
function random(mod) {
  var e = Math.random() * 1000;
  return (e - e % mod) / mod
}

QUnit.module("Call execute and exit");

//TODO Move to tests_for_framework
QUnit.test( "One processor test", function(assert) {
  var done = assert.async();
  var vm = createVirtualMachine(["#jump", 0, "#nop"]);
  vm.step(1);
  setTimeout(function() {
    assert.ok(vm.getProcessor(0).registers[0] == 2, "Processor is working");
    done();
    vm.stop();
  }, 100);
});

QUnit.test( "Two processors test", function(assert) {
  var done = assert.async();
  var vm = createVirtualMachine([
    "#jump", 0, "#nop", 
    "#jump", 0, "#nop"
  ]);
  var p = vm.getProcessor(0);
  vm.addProcessor(p,p.newRegisters(3, 0),0);
  vm.step(1);
  setTimeout(function() {
    assert.ok(vm.getProcessor(0).registers[0] == 2, "First processor is working");
    assert.ok(vm.getProcessor(1).registers[0] == 5, "Second processor is working");
    done();
    vm.stop();
  }, 100);
});

QUnit.test( "New processor registers state test", function(assert) {
  var done = assert.async();
  var program = [
    "#jump", 0, "#nop", 
    "#jump", 0, "#nop"
  ];
  for(var i = 0; i < REGISTERS_VOLUME; i++)
    program.push(random(2));
  var vm = createVirtualMachine(program);
  var p = vm.getProcessor(0);
  vm.addProcessor(p,p.newRegisters(3, 6),6);
  vm.step(1);
  setTimeout(function() {
    registers = vm.getProcessor(1).registers;
    for(var i = 1; i < registers.length; i++)
      assert.ok(registers[i] == program[i + 6], "Second processor register " + i + " value is right");  
    done();
    vm.stop();
  }, 100);
});

QUnit.test( "Many processors test", function(assert) {
  var done = assert.async();
  var number = Math.random()*100 + 1;
  var program = [];
  for(var i = 0; i < number; i++) {
    program.push("#jump");
    program.push(0);
    program.push("#nop");
  }
  var vm = createVirtualMachine(program);
  var p = vm.getProcessor(0);
  for(var i = 1; i < number; i++) {
    vm.addProcessor(p,p.newRegisters(i*3, 0),0);
  }
  vm.step(1);
  setTimeout(function() {
    for(var i = 0; i < number; i++) {
      assert.ok(vm.getProcessor(i).registers[0] == (i*3 + 2), i + "th processor is working");
    }
    done();
    vm.stop();
  }, 1000);
});

QUnit.test("Call execute test", function(assert) {
  var done = assert.async();
  var vm = createVirtualMachine([
    "#call_execute", 1, 2,
    "#nop", 
    "#jump", 0, "#nop"
  ]);
  vm.getProcessor(0).registers[1] = 0;
  vm.getProcessor(0).registers[2] = 4;
  vm.step(1);
  setTimeout(function() {
    assert.ok(vm.getProcessor(0).registers[0] == 3, "First processor is working");
    assert.ok(vm.getProcessor(1).registers[0] == 6, "Second processor is working");
    done();
    vm.stop();
  }, 100);
});

QUnit.test("Call execute registers state test", function(assert) {
  var done = assert.async();
  var program = [
    "#call_execute", 1, 2,
    "#nop", 
    "#jump", 0, "#nop"
  ];
  for(var i = 0; i < REGISTERS_VOLUME; i++)
    program.push(random(2));
  var vm = createVirtualMachine(program);
  vm.getProcessor(0).registers[1] = 7;
  vm.getProcessor(0).registers[2] = 4;
  vm.step(1);
  setTimeout(function() {
    registers = vm.getProcessor(1).registers;
    assert.ok(registers[0] == 6, "Second processor register " + 0 + " value is right");  
    for(var i = 1; i < registers.length; i++)
      assert.ok(registers[i] == program[i + 7], "Second processor register " + i + " value is right");  
    done();
    vm.stop();
  }, 100);
});

QUnit.test("Stop vm with two processors test", function(assert) {
  var done = assert.async();
  var vm = createVirtualMachine([
    "#jump", 0, "#nop", 
    "#jump", 0, "#nop"
  ]);
  var p = vm.getProcessor(0);
  vm.addProcessor(p,p.newRegisters(3, 0));
  vm.step(1);
  setTimeout(function() {
    vm.stop();
    assert.ok(!vm.getProcessor(0).process, "First processor is stopped");
    assert.ok(!vm.getProcessor(1).process, "Second processor is stopped");
    done();
  }, 100);
});

QUnit.test("Processor store test without pointer", function(assert) {
  var processor = new VirtualMachine.Processor({});
  //Check for empty function
  console.log(processor.store.toString());
  assert.ok(processor.store.toString().indexOf("{}") != -1);
});

QUnit.test("Processor store test with pointer", function(assert) {
  var projection = {state: {pointer: 10}};
  var processor = new VirtualMachine.Processor(projection);
  //Check for a real store function
  //TODO simplify this mechanism with the strategy pattern
  assert.ok(processor.store.toString().indexOf("this.registers[i]") != -1);
});

QUnit.test( "Two processors store test", function(assert) {
  var vm = createVirtualMachine([
    "#jump", 0, "#nop", 
    "#jump", 0, "#nop"
  ]);
  var p = vm.getProcessor(0);
  vm.addProcessor(p,p.newRegisters(3, 0),0);
  assert.ok(vm.getProcessor(1).store.toString().indexOf("this.registers[i]") != -1);
});

QUnit.test( "Processor registers storing", function(assert) {
  var vm = createVirtualMachine();
  var registers = vm.getProcessor(0).registers;
  for(var i = 0; i < registers.length; i++)
    registers[i] = random(2);
  vm.getProcessor(0).store();
  for(var i = 0; i < registers.length; i++)
    assert.ok(vm.manager.memory[i + config.core.processor[0].state.pointer] == registers[i], "Register " + i + " stored properly");
});

QUnit.test("Processor store 32 bit test", function(assert) {});

QUnit.test("Processor project pointer", function(assert) {});

QUnit.test("Registers storing after interruption test", function(assert) {
  var done = assert.async();
  var program = [
    "#call_execute", 1, 2,
    "#nop", 
  ]
  for(var i = 1; i < REGISTERS_VOLUME; i++)
    program = program.concat(["#load_value", random(2), i]);
  program.push("#nop");
  var vm = createVirtualMachine(program);
  vm.getProcessor(0).registers[1] = program.length;
  vm.getProcessor(0).registers[2] = 4;
  vm.step(1);
  setTimeout(function() {
    vm.stop();
    var registers = vm.getProcessor(1).registers;
    for(var i = 0; i < registers.length; i++)
      assert.ok(vm.manager.memory[i + program.length] == registers[i], "Register " + i + " stored properly");
    done();
  }, 1000);
});

//TODO destroy processor in machine
QUnit.test("Exit test", function(assert) {
  var done = assert.async();
  var vm = createVirtualMachine([
    "#call_execute", 1, 2,
    "#nop", 
    "#jump", 0, "#exit"
  ]);
  vm.step(1);
  vm.getProcessor(0).registers[1] = 0;
  vm.getProcessor(0).registers[2] = 4;
  setTimeout(function() {
    assert.ok(vm.getProcessor(0).process, "First processor is working");
    assert.ok(!vm.getProcessor(1), "Second processor is destroyed");
    done();
    vm.stop();
  }, 100);
});

// QUnit.test("Exit test with multiple processors", function(assert) {
//   var done = assert.async();
//   var number = Math.random()*100 + 1;
//   var program = ["#call_execute", 4, 0, "#nop"];
//   for(var i = 2; i < number - 1; i++) {
//     program.push("#call_execute");
//     program.push(i*4);
//     program.push(0);
//     program.push("#exit");
//   }
//   program.push("#exit");
//   var vm = createVirtualMachine(program);
//   vm.step(1);
//   setTimeout(function() {
//     assert.ok(vm.getProcessor(0).process, "Main processor is working");
//     for(var i = 1; i < number - 1; i++)
//       assert.ok(!vm.getProcessor(i).process, i + "th processor is stopped");
//     done();
//     vm.stop();
//   }, 1000);
// });
//Machine stop

//Virtual machine inject

//Virtual machine project


// Test example should
// Create a processor with a task:
  //To sum all registers
  //To save this value in 1 register
  //To exit
//